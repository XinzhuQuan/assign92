﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using WindowsFormsApp4;

namespace WindowsFormsApp4
{
    static class Program
    {
        static void Main()
        {
            //initialize a new thread 
            //this thread starts your GUI Form1
            Thread A = new Thread(GUII);
            A.Start();//start the thread and join
            A.Join();

        }

       
        //instan. the  Form1
        public static Form1 GUI = new Form1();

        //This delegate increase prograss bar amount
        //it has a parameter 
        delegate void progresscheck(int amount);
      //this delegate do not have parameter
        delegate void finish();

        static void GUII()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(GUI);//run form 1 here
        }

     
        //When this method is called in your option class, it incress the progress
        public static void increase(int input)
        {
            //Test a control component on the Form1 to see if invoke is required.
            if (Program.GUI.progressBar1.InvokeRequired)
            {
                //if invoke required, begin invoke and increase the bar
                progresscheck progressdelegate = new progresscheck(progress_check_method);
                GUI.progressBar1.BeginInvoke(progressdelegate, input);
            }
            else
            {
                //if invoke is not necessary, just increase the bar.
              
                progress_check_method(input);
            }

        }

        //When the method is called. finish function in Form1 will pull the output result and show the result at testbox6
        public static void finished()
        {
            //Test a control component on the Form1 to see if invoke is required.
            if (Program.GUI.textBox6.InvokeRequired)
            {
                finish finishdelegate = new finish(finish_check_method);
                GUI.textBox6.BeginInvoke(finishdelegate);
            }
            else
            {
                finish_check_method();
            }


        }

        
        //it increse the bar amount directly
        public static void progress_check_method(int amout)
        {
            GUI.progressBar1.Value += amout;
            GUI.progressBar1.Update();
        }

        //it calls finish method directly
        public static void finish_check_method()
        {
            GUI.finish();

        }
    

}
}
