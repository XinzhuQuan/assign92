﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp4
{
    public class Fancyrandom
    {
        public Fancyrandom()
        {
            Polar();
        }
        public double[,] randlist;
        public void Polar()// output a random number matrix
        {
            Random rnd = new Random(Guid.NewGuid().GetHashCode());
            this.randlist = new double[CommonValue.trail*(CommonValue.indicator+1), CommonValue.stt+1];

            for (int i = 0; i <= CommonValue.trail - 1; i++)
            {
                for (int j = 0; j <= CommonValue.stt; j++)
                {
                    double x1;//there is a different range need to adjust
                    double x2;
                    double w;
                    do// use while to decide 
                    {
                        x1 = 2 * rnd.NextDouble() - 1;
                        x2 = 2 * rnd.NextDouble() - 1;
                        w = x1 * x1 + x2 * x2;
                    } while (w > 1);

                    double c = Math.Sqrt((-2 * Math.Log(w)) / w);
                    double z1 = c * x1;
                    double z2 = c * x2;
                    randlist[i, j] = z1;
                    if (CommonValue.indicator == 1)
                    {
                        randlist[i + CommonValue.trail, j] = -z1;
                    }
                }

            }
        }
    }
}
