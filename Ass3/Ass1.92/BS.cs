﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp4
{
    static class BS
    {
        static private double d1(double s, double k, double r, double v, double T, double t)
        {
            double result = 0;
            result = (Math.Log(s / k) + (r + v * v / 2.0) * (T - t)) / (v * Math.Sqrt(T - t));
            return result;
        }

        static public double delta(double u , double s, double k, double r, double v, double T, double t)
        {
            double result = 0;
            if (u == 1)
            {
                result = cdf(d1(s, k, r, v, T, t));
            }
            else if (u == -1)
            {
                result = cdf(d1(s, k, r, v, T, t)) - 1;
            }
            return result;
        }
        static private double cdf(double z)
        {
            double p = 0.3275911;
            double a1 = 0.254829592;
            double a2 = -0.284496736;
            double a3 = 1.421413741;
            double a4 = -1.453152027;
            double a5 = 1.061405429;

            int sign;
            if (z < 0.0)
                sign = -1;
            else
                sign = 1;
            double x = Math.Abs(z) / Math.Sqrt(2.0);
            double t = 1.0 / (1.0 + p * x);
            double erf = 1.0 - (((((a5 * t + a4) * t) + a3) * t + a2) * t + a1) * t * Math.Exp(-x * x);
            return 0.5 * (1.0 + sign * erf);
        }
    }
}
