﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp4
{
    class European : Option
    {// create the option class
        //public European(int a)
        //{
        //    this.trails = a;//constrator trails are the rows
        //}
        public double price = 0;
        public double CT = 0;
        public double option_price = 0;
       
        public double delta1 = 0;
 
        private double t = 0;
        public double cvone = 0;
        public double sum = 0;
        public double sum_CT = 0;
        public double sum_CT2 = 0;
        public double sum_deltain = 0;
        public double sum_deltade= 0;
        public double sum_vega = 0;
        public double sum_rho = 0;
        public double price_in = 0;
        public double price_de = 0;
        public double price_theta = 0;
        public double price_delta = 0;
        public double price_vega = 0;
        public double price_rho = 0;
         public double[] Price;
        

       
        public int trails;
      //  public double dt = Convert.ToDouble(CommonValue.Time) / Convert.ToDouble (CommonValue.steps);//calculte t
       // public double dtt = Convert.ToDouble(CommonValue.Time+CommonValue.increR* CommonValue.Time) / Convert.ToDouble(CommonValue.steps);//calculte the t+0.0001t
        
        public void GetPrice (double S,double K,double R,double T,double sigma)
        {
            double dt = Convert.ToDouble(T) / Convert.ToDouble(CommonValue.steps);
            double erddt = Math.Exp(R * dt);
            Price = new double[CommonValue.trail * (1 + CommonValue.indicator)];

            for (int trails = 0; trails < CommonValue.trail * (1 + CommonValue.indicator); trails++)
            {
               
                for (int idx = 1; idx <= CommonValue.steps; idx++)
                {   //use monto carlo calculate the stock price
                    //t = (idx - 1) * dt;
                    //   delta1 = BS.delta(CommonValue.type, CommonValue.St[trails, idx-1], CommonValue.Strike, CommonValue.Rate, CommonValue.Sigma, CommonValue.Time, t);
                    // delta2 = BS.delta(CommonValue.type, St2, CommonValue.Strike, CommonValue.Rate, CommonValue.Sigma, CommonValue.Time, t);
                    CommonValue.St[trails, 0] = S;
                    CommonValue.St[trails, idx] = CommonValue.St[trails, idx - 1] * Math.Exp(((R - 0.5 * sigma * sigma) * dt) + (sigma * Math.Sqrt(dt) * CommonValue.rand[trails, idx - 1]));
                    t = (idx - 1) * dt;
                    delta1 = BS.delta(CommonValue.type, CommonValue.St[trails, idx - 1], K, R, sigma, T , t);
                    cvone = cvone+ delta1 * (CommonValue.St[trails, idx] - CommonValue.St[trails, idx - 1] * erddt);

                }

                Price[trails] = Math.Max(0, CommonValue.type * (CommonValue.St[trails, CommonValue.steps] - K)); //use the last column price minus strike price

                if (CommonValue.indicator2 == 1)// if it use delta reduction method, the last column of option price should have some changes
                  {
                    Price[trails] = Math.Max(0, CommonValue.type * (CommonValue.St[trails, CommonValue.steps] - K)) - 1 * cvone;
                  }
                cvone = 0;
                

                sum = sum + Price[trails];
               
                //price_in = Math.Max(0, CommonValue.type * (CommonValue.St[trails, CommonValue.steps] / CommonValue.Spotprice * (CommonValue.Spotprice + CommonValue.increR) - CommonValue.Strike));//use the last column's stock price  in order to calculate the delta
                //price_de = Math.Max(0, CommonValue.type * (CommonValue.St[trails, CommonValue.steps] / CommonValue.Spotprice * (CommonValue.Spotprice - CommonValue.increR) - CommonValue.Strike));//use the last column's stock  price in order to calculate the delta
                //price_vega = Math.Max(0, CommonValue.type * (S * Math.Exp((CommonValue.Rate - 0.5 * (sigma + CommonValue.increS) * (sigma + CommonValue.increS)) * T + (sigma + CommonValue.increS) / sigma * ((Math.Log(CommonValue.St[trails, CommonValue.steps] / S)) - (CommonValue.Rate - 0.5 * CommonValue.Sigma * CommonValue.Sigma) * CommonValue.Time)) - CommonValue.Strike));//use the last column change of volatility's price minus strike price
                //price_rho = Math.Max(0, CommonValue.type * (CommonValue.St[trails, CommonValue.steps] * Math.Exp(CommonValue.increS * T) - CommonValue.Strike));//use the last column change of rate's price minus strike price
                //sum = sum + CommonValue.St[trails, CommonValue.steps];
                //sum_deltain = sum_deltain + price_in;//sum al the trails of the undiscounted greeks
                //sum_deltade = sum_deltade + price_de;
                //sum_vega = sum_vega + price_vega;
                //sum_rho = sum_rho + price_rho;


            }

            option_price = sum * Math.Exp(- T * R) / (CommonValue.trail * (1 + CommonValue.indicator));
            //sum_deltain = sum_deltain * Math.Exp(-T* R) / (CommonValue.trail * (1 + CommonValue.indicator));//discount the increase price
            //sum_deltade = sum_deltade * Math.Exp(-T * R) / (CommonValue.trail * (1 + CommonValue.indicator));//discount the decrease price
            //sum_vega = sum_vega * Math.Exp(-T * R) / (CommonValue.trail * (1 + CommonValue.indicator));//discount
            //sum_rho = sum_rho * Math.Exp(-T * (CommonValue.increS + R)) / (CommonValue.trail * (1 + CommonValue.indicator));//discoun
           


        }

    }
}