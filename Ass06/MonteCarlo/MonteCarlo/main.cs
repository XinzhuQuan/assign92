﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace A
{
    public class main
    {
        static public double[] calculate(double S,double K,double r,int T, double sigma,double rebate,double barrier,double type, int downorup,int outorin, string option_Type)
        {
            CommonValue.Spotprice = S;
            CommonValue.Strike = K;
            CommonValue.Rate = r;
            CommonValue.Time = T;
            CommonValue.Sigma = sigma;
            CommonValue.rebate = rebate;
            CommonValue.barrier = barrier;
            CommonValue.type = type;
            CommonValue.downorup = downorup;
            CommonValue.outorin = outorin;
            CommonValue.gene_path();
            double[] a = new double[7];
            switch (option_Type)
            {
                case "EuropeanOption":
                     a=Simulator.EuropeanOption();
                    break;
                 
                case "AsianOption":
                    a=Simulator.AsianOption();
                    break;
                case "RangeOption":
                    a=Simulator.RangeOption();
                    break;
                case "LookbackOption":
                    a=Simulator.LookBackOprion();
                    break;
                case "BarrierOption":
                    a=Simulator.BarrierOption();
                    break;
                case "DigitalOption":
                    a=Simulator.DigitalOption();
                    break;
              
            }
            return a;
        }
      //  public enum string { EuropeanOption, AsianOption, RangeOption, LookbackOption, BarrierOption, DigitalOption }
    }
    
}
