﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace A
{
    public class Simulator
    {
        static public  double[] AsianOption()
            {
                double[] result;
                Asian Eu = new Asian();//sum every trails' price 
                Asian Eudi = new Asian();
                Asian Eudd = new Asian();
                Asian Euv = new Asian();
                Asian Eut = new Asian();
                Asian Eur = new Asian();
                Eu.GetPrice (CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate, CommonValue.Time, CommonValue.Sigma);
                Eudi.GetPrice(CommonValue.Spotprice + CommonValue.increR * CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate, CommonValue.Time, CommonValue.Sigma);          
                Eudd.GetPrice(CommonValue.Spotprice - CommonValue.increR * CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate, CommonValue.Time, CommonValue.Sigma);           
                Eut.GetPrice(CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate, CommonValue.Time + CommonValue.Time * CommonValue.increS, CommonValue.Sigma);            
                Euv.GetPrice(CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate, CommonValue.Time, CommonValue.Sigma + CommonValue.Sigma * CommonValue.increS);            
                Eur.GetPrice(CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate + CommonValue.Rate * CommonValue.increS, CommonValue.Time, CommonValue.Sigma);               
                double option_price = Eu.option_price;
                double sum_CT = 0;
                double sum_CT2 = 0;
                double option_price_deltain = Eudi.option_price;
                double option_price_deltade = Eudd.option_price;
                double option_price_theta = Eut.option_price;
                double option_price_vega = Euv.option_price;
                double option_price_rho = Eur.option_price;
                double delta = 0;
                double gamma = 0;
                double theta = 0;
                double vega = 0;
                double rho = 0;
                double sum_ave = 0;
                double SD = 0;
                double SE = 0;
                //List<double> store = new List<double>();

                sum_ave = Eu.sum / (CommonValue.trail * (1 + CommonValue.indicator)); //calculate the average of undiscounted option price

                for (int i = 0; i < CommonValue.trail; i++)//arthtic and not arthetic
                {

                    if (CommonValue.indicator == 1)//antithetic
                    {
                        Eu.Price[i] = 0.5 * (Eu.Price[i] + Eu.Price[i + CommonValue.trail]);  //change the first M option price,no matter it is normal or delta(deal with that is European clas
                    }
                    if (CommonValue.indicator2 == 0)//not delta
                    {
                        SD = SD + (Eu.Price[i] - sum_ave) * (Eu.Price[i] - sum_ave);

                    }

                }


                if (CommonValue.indicator2 == 1) //if delta reduction is true, use another formula to calculate the SD
                {
                    for (int i = 0; i < CommonValue.trail; i++)
                    {

                        sum_CT = sum_CT + Eu.Price[i];
                        sum_CT2 = sum_CT2 + Eu.Price[i] * Eu.Price[i];
                        //store.Add(Eu.Price[i] * Eu.Price[i]);

                    }


                    SD = sum_CT2 - sum_CT * sum_CT / CommonValue.trail;
                }
                SE = Math.Sqrt((SD) * Math.Exp(-2 * CommonValue.Rate * CommonValue.Time) / (CommonValue.trail - 1) / CommonValue.trail);
                delta = (option_price_deltain - option_price_deltade) / (2 * CommonValue.Spotprice * CommonValue.increR);//calclulate the delta
                gamma = (option_price_deltain + option_price_deltade - 2 * option_price) / (CommonValue.Spotprice * CommonValue.Spotprice * CommonValue.increR * CommonValue.increR);//calculate the gamma
                vega = (option_price_vega - option_price) / (CommonValue.increS * CommonValue.Sigma);
                theta = -(option_price_theta - option_price) / (CommonValue.increS * CommonValue.Time);//calculate
                rho = (option_price_rho - option_price) / (CommonValue.increS * CommonValue.Rate);

                return result = new double[7] { option_price, delta, gamma, theta, vega, rho, SE };
            }

        static public double[] BarrierOption()
            { 
                double[] result;
                barrier Eu = new barrier();//sum every trails' price
                barrier Eudi = new barrier();
                barrier Eudd = new barrier();
                barrier Euv = new barrier();
                barrier Eut = new barrier();
                barrier Eur = new barrier();
                Eu.GetPrice(CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate, CommonValue.Time, CommonValue.Sigma);
                
                Eudi.GetPrice(CommonValue.Spotprice + CommonValue.increR * CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate, CommonValue.Time, CommonValue.Sigma);
            
                Eudd.GetPrice(CommonValue.Spotprice - CommonValue.increR * CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate, CommonValue.Time, CommonValue.Sigma);
            
                Eut.GetPrice(CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate, CommonValue.Time + CommonValue.Time * CommonValue.increS, CommonValue.Sigma);
              
                Euv.GetPrice(CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate, CommonValue.Time, CommonValue.Sigma + CommonValue.Sigma * CommonValue.increS);
               
                Eur.GetPrice(CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate + CommonValue.Rate * CommonValue.increS, CommonValue.Time, CommonValue.Sigma);
               
                double option_price = Eu.option_price;
                double sum_CT = 0;
                double sum_CT2 = 0;
                double option_price_deltain = Eudi.option_price;
                double option_price_deltade = Eudd.option_price;
                double option_price_theta = Eut.option_price;
                double option_price_vega = Euv.option_price;
                double option_price_rho = Eur.option_price;
                double delta = 0;
                double gamma = 0;
                double theta = 0;
                double vega = 0;
                double rho = 0;
                double sum_ave = 0;
                double SD = 0;
                double SE = 0;
                //List<double> store = new List<double>();

                sum_ave = Eu.sum / (CommonValue.trail * (1 + CommonValue.indicator)); //calculate the average of undiscounted option price

                for (int i = 0; i < CommonValue.trail; i++)//arthtic and not arthetic
                {

                    if (CommonValue.indicator == 1)//antithetic
                    {
                        Eu.Price[i] = 0.5 * (Eu.Price[i] + Eu.Price[i + CommonValue.trail]);  //change the first M option price,no matter it is normal or delta(deal with that is European clas
                    }
                    if (CommonValue.indicator2 == 0)//not delta
                    {
                        SD = SD + (Eu.Price[i] - sum_ave) * (Eu.Price[i] - sum_ave);

                    }

                }

                if (CommonValue.indicator2 == 1) //if delta reduction is true, use another formula to calculate the SD
                {
                    for (int i = 0; i < CommonValue.trail; i++)
                    {

                        sum_CT = sum_CT + Eu.Price[i];
                        sum_CT2 = sum_CT2 + Eu.Price[i] * Eu.Price[i];
                        //store.Add(Eu.Price[i] * Eu.Price[i]);

                    }


                    SD = sum_CT2 - sum_CT * sum_CT / CommonValue.trail;
                }
                SE = Math.Sqrt((SD) * Math.Exp(-2 * CommonValue.Rate * CommonValue.Time) / (CommonValue.trail - 1) / CommonValue.trail);
                delta = (option_price_deltain - option_price_deltade) / (2 * CommonValue.Spotprice * CommonValue.increR);//calclulate the delta
                gamma = (option_price_deltain + option_price_deltade - 2 * option_price) / (CommonValue.Spotprice * CommonValue.Spotprice * CommonValue.increR * CommonValue.increR);//calculate the gamma
                vega = (option_price_vega - option_price) / (CommonValue.increS * CommonValue.Sigma);
                theta = -(option_price_theta - option_price) / (CommonValue.increS * CommonValue.Time);//calculate
                rho = (option_price_rho - option_price) / (CommonValue.increS * CommonValue.Rate);

                

                return result = new double[7] { option_price, delta, gamma, theta, vega, rho, SE };
            }

        static public double[] RangeOption()
            {
            double[] result;
            Range Eu = new Range();//sum every trails' price
                Range Eudi = new Range();
                Range Eudd = new Range();
                Range Euv = new Range();
                Range Eut = new Range();
                Range Eur = new Range();
                Eu.GetPrice(CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate, CommonValue.Time, CommonValue.Sigma);
               
                Eudi.GetPrice(CommonValue.Spotprice + CommonValue.increR * CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate, CommonValue.Time, CommonValue.Sigma);
               
                Eudd.GetPrice(CommonValue.Spotprice - CommonValue.increR * CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate, CommonValue.Time, CommonValue.Sigma);
                
                Eut.GetPrice(CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate, CommonValue.Time + CommonValue.Time * CommonValue.increS, CommonValue.Sigma);
               
                Euv.GetPrice(CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate, CommonValue.Time, CommonValue.Sigma + CommonValue.Sigma * CommonValue.increS);
              
                Eur.GetPrice(CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate + CommonValue.Rate * CommonValue.increS, CommonValue.Time, CommonValue.Sigma);
                
                double option_price = Eu.option_price;
                double sum_CT = 0;
                double sum_CT2 = 0;
                double option_price_deltain = Eudi.option_price;
                double option_price_deltade = Eudd.option_price;
                double option_price_theta = Eut.option_price;
                double option_price_vega = Euv.option_price;
                double option_price_rho = Eur.option_price;
                double delta = 0;
                double gamma = 0;
                double theta = 0;
                double vega = 0;
                double rho = 0;
                double sum_ave = 0;
                double SD = 0;
                double SE = 0;
                sum_ave = Eu.sum / (CommonValue.trail * (1 + CommonValue.indicator)); //calculate the average of undiscounted option price

                for (int i = 0; i < CommonValue.trail; i++)//arthtic and not arthetic
                {

                    if (CommonValue.indicator == 1)//antithetic
                    {
                        Eu.Price[i] = 0.5 * (Eu.Price[i] + Eu.Price[i + CommonValue.trail]);  //change the first M option price,no matter it is normal or delta(deal with that is European clas
                    }
                    if (CommonValue.indicator2 == 0)//not delta
                    {
                        SD = SD + (Eu.Price[i] - sum_ave) * (Eu.Price[i] - sum_ave);

                    }

                }

                if (CommonValue.indicator2 == 1) //if delta reduction is true, use another formula to calculate the SD
                {
                    for (int i = 0; i < CommonValue.trail; i++)
                    {

                        sum_CT = sum_CT + Eu.Price[i];
                        sum_CT2 = sum_CT2 + Eu.Price[i] * Eu.Price[i];
                        //store.Add(Eu.Price[i] * Eu.Price[i]);

                    }

                    SD = sum_CT2 - sum_CT * sum_CT / CommonValue.trail;
                }
                SE = Math.Sqrt((SD) * Math.Exp(-2 * CommonValue.Rate * CommonValue.Time) / (CommonValue.trail - 1) / CommonValue.trail);
                delta = (option_price_deltain - option_price_deltade) / (2 * CommonValue.Spotprice * CommonValue.increR);//calclulate the delta
                gamma = (option_price_deltain + option_price_deltade - 2 * option_price) / (CommonValue.Spotprice * CommonValue.Spotprice * CommonValue.increR * CommonValue.increR);//calculate the gamma
                vega = (option_price_vega - option_price) / (CommonValue.increS * CommonValue.Sigma);
                theta = -(option_price_theta - option_price) / (CommonValue.increS * CommonValue.Time);//calculate
                rho = (option_price_rho - option_price) / (CommonValue.increS * CommonValue.Rate);

               

                return result = new double[7] { option_price, delta, gamma, theta, vega, rho, SE };
            }

            static public double[] LookBackOprion()
            {
               double[] result;
               Lookback Eu = new Lookback();//sum every trails' price
                Lookback Eudi = new Lookback();
                Lookback Eudd = new Lookback();
                Lookback Euv = new Lookback();
                Lookback Eut = new Lookback();
                Lookback Eur = new Lookback();
                Eu.GetPrice(CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate, CommonValue.Time, CommonValue.Sigma);
               
                Eudi.GetPrice(CommonValue.Spotprice + CommonValue.increR * CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate, CommonValue.Time, CommonValue.Sigma);
               
                Eudd.GetPrice(CommonValue.Spotprice - CommonValue.increR * CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate, CommonValue.Time, CommonValue.Sigma);
                
                Eut.GetPrice(CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate, CommonValue.Time + CommonValue.Time * CommonValue.increS, CommonValue.Sigma);
               
                Euv.GetPrice(CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate, CommonValue.Time, CommonValue.Sigma + CommonValue.Sigma * CommonValue.increS);
                
                Eur.GetPrice(CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate + CommonValue.Rate * CommonValue.increS, CommonValue.Time, CommonValue.Sigma);
               
                double option_price = Eu.option_price;
                double sum_CT = 0;
                double sum_CT2 = 0;
                double option_price_deltain = Eudi.option_price;
                double option_price_deltade = Eudd.option_price;
                double option_price_theta = Eut.option_price;
                double option_price_vega = Euv.option_price;
                double option_price_rho = Eur.option_price;
                double delta = 0;
                double gamma = 0;
                double theta = 0;
                double vega = 0;
                double rho = 0;
                double sum_ave = 0;
                double SD = 0;
                double SE = 0;
                //List<double> store = new List<double>();

                sum_ave = Eu.sum / (CommonValue.trail * (1 + CommonValue.indicator)); //calculate the average of undiscounted option price

                for (int i = 0; i < CommonValue.trail; i++)//arthtic and not arthetic
                {

                    if (CommonValue.indicator == 1)//antithetic
                    {
                        Eu.Price[i] = 0.5 * (Eu.Price[i] + Eu.Price[i + CommonValue.trail]);  //change the first M option price,no matter it is normal or delta(deal with that is European clas
                    }
                    if (CommonValue.indicator2 == 0)//not delta
                    {
                        SD = SD + (Eu.Price[i] - sum_ave) * (Eu.Price[i] - sum_ave);

                    }

                }

                if (CommonValue.indicator2 == 1) //if delta reduction is true, use another formula to calculate the SD
                {
                    for (int i = 0; i < CommonValue.trail; i++)
                    {

                        sum_CT = sum_CT + Eu.Price[i];
                        sum_CT2 = sum_CT2 + Eu.Price[i] * Eu.Price[i];
                        //store.Add(Eu.Price[i] * Eu.Price[i]);

                    }
                    SD = sum_CT2 - sum_CT * sum_CT / CommonValue.trail;
                }
                SE = Math.Sqrt((SD) * Math.Exp(-2 * CommonValue.Rate * CommonValue.Time) / (CommonValue.trail - 1) / CommonValue.trail);
                delta = (option_price_deltain - option_price_deltade) / (2 * CommonValue.Spotprice * CommonValue.increR);//calclulate the delta
                gamma = (option_price_deltain + option_price_deltade - 2 * option_price) / (CommonValue.Spotprice * CommonValue.Spotprice * CommonValue.increR * CommonValue.increR);//calculate the gamma
                vega = (option_price_vega - option_price) / (CommonValue.increS * CommonValue.Sigma);
                theta = -(option_price_theta - option_price) / (CommonValue.increS * CommonValue.Time);//calculate
                rho = (option_price_rho - option_price) / (CommonValue.increS * CommonValue.Rate);
              
                return result = new double[7] { option_price, delta, gamma, theta, vega, rho, SE };
            }

          static public double[] DigitalOption()
        { 
                double[] result;
                digital Eu = new digital();//sum every trails' price
                digital Eudi = new digital();
                digital Eudd = new digital();
                digital Euv = new digital();
                digital Eut = new digital();
                digital Eur = new digital();
                Eu.GetPrice(CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate, CommonValue.Time, CommonValue.Sigma);  
                Eudi.GetPrice(CommonValue.Spotprice + CommonValue.increR * CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate, CommonValue.Time, CommonValue.Sigma);              
                Eudd.GetPrice(CommonValue.Spotprice - CommonValue.increR * CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate, CommonValue.Time, CommonValue.Sigma);               
                Eut.GetPrice(CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate, CommonValue.Time + CommonValue.Time * CommonValue.increS, CommonValue.Sigma);              
                Euv.GetPrice(CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate, CommonValue.Time, CommonValue.Sigma + CommonValue.Sigma * CommonValue.increS);               
                Eur.GetPrice(CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate + CommonValue.Rate * CommonValue.increS, CommonValue.Time, CommonValue.Sigma);
               
                double option_price = Eu.option_price;
                double sum_CT = 0;
                double sum_CT2 = 0;
                double option_price_deltain = Eudi.option_price;
                double option_price_deltade = Eudd.option_price;
                double option_price_theta = Eut.option_price;
                double option_price_vega = Euv.option_price;
                double option_price_rho = Eur.option_price;
                double delta = 0;
                double gamma = 0;
                double theta = 0;
                double vega = 0;
                double rho = 0;
                double sum_ave = 0;
                double SD = 0;
                double SE = 0;
                //List<double> store = new List<double>();

                sum_ave = Eu.sum / (CommonValue.trail * (1 + CommonValue.indicator)); //calculate the average of undiscounted option price

                for (int i = 0; i < CommonValue.trail; i++)//arthtic and not arthetic
                {

                    if (CommonValue.indicator == 1)//antithetic
                    {
                        Eu.Price[i] = 0.5 * (Eu.Price[i] + Eu.Price[i + CommonValue.trail]);  //change the first M option price,no matter it is normal or delta(deal with that is European clas
                    }
                    if (CommonValue.indicator2 == 0)//not delta
                    {
                        SD = SD + (Eu.Price[i] - sum_ave) * (Eu.Price[i] - sum_ave);

                    }

                }

                if (CommonValue.indicator2 == 1) //if delta reduction is true, use another formula to calculate the SD
                {
                    for (int i = 0; i < CommonValue.trail; i++)
                    {

                        sum_CT = sum_CT + Eu.Price[i];
                        sum_CT2 = sum_CT2 + Eu.Price[i] * Eu.Price[i];
                        //store.Add(Eu.Price[i] * Eu.Price[i]);

                    }


                    SD = sum_CT2 - sum_CT * sum_CT / CommonValue.trail;
                }
                SE = Math.Sqrt((SD) * Math.Exp(-2 * CommonValue.Rate * CommonValue.Time) / (CommonValue.trail - 1) / CommonValue.trail);
                delta = (option_price_deltain - option_price_deltade) / (2 * CommonValue.Spotprice * CommonValue.increR);//calclulate the delta
                gamma = (option_price_deltain + option_price_deltade - 2 * option_price) / (CommonValue.Spotprice * CommonValue.Spotprice * CommonValue.increR * CommonValue.increR);//calculate the gamma
                vega = (option_price_vega - option_price) / (CommonValue.increS * CommonValue.Sigma);
                theta = -(option_price_theta - option_price) / (CommonValue.increS * CommonValue.Time);//calculate
                rho = (option_price_rho - option_price) / (CommonValue.increS * CommonValue.Rate);

               
                return result = new double[7] { option_price, delta, gamma, theta, vega, rho, SE };

        }

            static public double[] EuropeanOption()
            {  double[] result;
                European Eu = new European();//sum every trails' price
                European Eudi = new European();
                European Eudd = new European();
                European Euv = new European();
                European Eut = new European();
                European Eur = new European();

                Eu.GetPrice(CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate, CommonValue.Time, CommonValue.Sigma);              
                Eudi.GetPrice(CommonValue.Spotprice + CommonValue.increR * CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate, CommonValue.Time, CommonValue.Sigma);             
                Eudd.GetPrice(CommonValue.Spotprice - CommonValue.increR * CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate, CommonValue.Time, CommonValue.Sigma);
                Eut.GetPrice(CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate, CommonValue.Time + CommonValue.Time * CommonValue.increS, CommonValue.Sigma);
                Euv.GetPrice(CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate, CommonValue.Time, CommonValue.Sigma + CommonValue.Sigma * CommonValue.increS);
                Eur.GetPrice(CommonValue.Spotprice, CommonValue.Strike, CommonValue.Rate + CommonValue.Rate * CommonValue.increS, CommonValue.Time, CommonValue.Sigma);
               
                double option_price = Eu.option_price;
                double sum_CT = 0;
                double sum_CT2 = 0;
                double option_price_deltain = Eudi.option_price;
                double option_price_deltade = Eudd.option_price;
                double option_price_theta = Eut.option_price;
                double option_price_vega = Euv.option_price;
                double option_price_rho = Eur.option_price;
                double delta = 0;
                double gamma = 0;
                double theta = 0;
                double vega = 0;
                double rho = 0;
                double sum_ave = 0;
                double SD = 0;
                double SE = 0;
                //List<double> store = new List<double>();
                sum_ave = Eu.sum / (CommonValue.trail * (1 + CommonValue.indicator)); //calculate the average of undiscounted option price

                for (int i = 0; i < CommonValue.trail; i++)//arthtic and not arthetic
                {

                    if (CommonValue.indicator == 1)//antithetic
                    {
                        Eu.Price[i] = 0.5 * (Eu.Price[i] + Eu.Price[i + CommonValue.trail]);  //change the first M option price,no matter it is normal or delta(deal with that is European clas
                    }
                    if (CommonValue.indicator2 == 0)//not delta
                    {
                        SD = SD + (Eu.Price[i] - sum_ave) * (Eu.Price[i] - sum_ave);

                    }

                }

                if (CommonValue.indicator2 == 1) //if delta reduction is true, use another formula to calculate the SD
                {
                    for (int i = 0; i < CommonValue.trail; i++)
                    {

                        sum_CT = sum_CT + Eu.Price[i];
                        sum_CT2 = sum_CT2 + Eu.Price[i] * Eu.Price[i];
                        //store.Add(Eu.Price[i] * Eu.Price[i]);

                    }


                    SD = sum_CT2 - sum_CT * sum_CT / CommonValue.trail;
                }
                SE = Math.Sqrt((SD) * Math.Exp(-2 * CommonValue.Rate * CommonValue.Time) / (CommonValue.trail - 1) / CommonValue.trail);
                delta = (option_price_deltain - option_price_deltade) / (2 * CommonValue.Spotprice * CommonValue.increR);//calclulate the delta
                gamma = (option_price_deltain + option_price_deltade - 2 * option_price) / (CommonValue.Spotprice * CommonValue.Spotprice * CommonValue.increR * CommonValue.increR);//calculate the gamma
                vega = (option_price_vega - option_price) / (CommonValue.increS * CommonValue.Sigma);
                theta = -(option_price_theta - option_price) / (CommonValue.increS * CommonValue.Time);//calculate
                rho = (option_price_rho - option_price) / (CommonValue.increS * CommonValue.Rate);
                
                return result = new double[7] { option_price, delta, gamma, theta, vega, rho, SE };//output }

            }

        }
    }


