﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace A

{
    static class Ienum
    {
        public static IEnumerable<long> Step(long startIndex, long endIndex, long stepSize)
        {
            for (long i = startIndex; i < endIndex; i = i + stepSize)
            {
                yield return i;
            }
        }
    }
}
