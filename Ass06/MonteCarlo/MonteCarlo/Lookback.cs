﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace A
{
    public class Lookback : Option
    {
        public double sum = 0;
        public double option_price = 0;
        public double[] Price;
        public void GetPrice(double S, double K, double R, double T, double sigma)
        {

            double dt = Convert.ToDouble(T) / Convert.ToDouble(CommonValue.steps);
            double erddt = Math.Exp(R * dt);
            Price = new double[CommonValue.trail * (1 + CommonValue.indicator)];
            double delta1 = 0;
            double t = 0;
            double cvone = 0;

           
            Parallel.ForEach(Ienum.Step(0, CommonValue.trail * (1 + CommonValue.indicator), 1), new ParallelOptions { MaxDegreeOfParallelism = CommonValue.thread }, trails =>
            {
                double maxprice;
                double minprice;
                maxprice = CommonValue.Spotprice;
                minprice = CommonValue.Spotprice;
                for (int idx = 1; idx <= CommonValue.steps; idx++)
                {   //use monto carlo calculate the stock price
                    //t = (idx - 1) * dt;
                    //   delta1 = BS.delta(CommonValue.type, CommonValue.St[trails, idx-1], CommonValue.Strike, CommonValue.Rate, CommonValue.Sigma, CommonValue.Time, t);
                    // delta2 = BS.delta(CommonValue.type, St2, CommonValue.Strike, CommonValue.Rate, CommonValue.Sigma, CommonValue.Time, t);
                    CommonValue.St[trails, 0] = S;
                    CommonValue.St[trails, idx] = CommonValue.St[trails, idx - 1] * Math.Exp(((R - 0.5 * sigma * sigma) * dt) + (sigma * Math.Sqrt(dt) * CommonValue.rand[trails, idx - 1]));
                    t = (idx - 1) * dt;
                    delta1 = BS.delta(CommonValue.type, CommonValue.St[trails, idx - 1], K, R, sigma, T, t);
                    cvone = cvone + delta1 * (CommonValue.St[trails, idx] - CommonValue.St[trails, idx - 1] * erddt);
                    maxprice = CommonValue.St[trails, idx] > maxprice ? CommonValue.St[trails, idx] : maxprice;
                    minprice = CommonValue.St[trails, idx] < minprice ? CommonValue.St[trails, idx] : minprice;
                }

                if (CommonValue.type == 1)
                {
                    Price[trails] = Math.Max(0,  (maxprice - K));
                }
                else
                {
                    Price[trails] = Math.Max(0, (K - minprice));
                }

                if (CommonValue.indicator2 == 1)// if it use delta reduction method, the last column of option price should have some changes
                {
                    if (CommonValue.type == 1)
                    {
                        Price[trails] = Math.Max(0, (maxprice - K)) - 1 * cvone;
                    }
                    else
                    {
                        Price[trails] = Math.Max(0, (K - minprice)) - 1 * cvone;
                    }
                }
                cvone = 0;
                sum = sum + Price[trails];
            });

          

                
                option_price = sum * Math.Exp(-T * R) / (CommonValue.trail * (1 + CommonValue.indicator));
            }



        }
    }

