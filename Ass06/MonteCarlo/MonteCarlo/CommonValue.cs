﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace A
{
    //create a class to put some universal variables
    static public class CommonValue
    { //store the value from the users
        static public double Spotprice;
        static public double Strike;
        static public int Time;
        static public double Sigma;
        static public double Rate;
        static public int trail=10000;
        static public int steps=100;
        static public double type;
        static public double rebate;
        static public double barrier;
       // static public int Cpu_cores;

        static public double[,] rand;
        //random matrix
        static public double[,] St;
        //stock matrix
      
        static public int indicator=0;
        static public int indicator2=0;
        static public int downorup; 
        static public int outorin;
        static public int stt;
        static public int thread=1;
        static public double increS = 0.001;
        static public double increR = 0.001;
        
        static public void gene_path()//create value in the matrix
        {
            stt = Convert.ToInt32((CommonValue.steps));
            St = new double[CommonValue.trail * (1 + CommonValue.indicator), stt + 1];
            rand = new Fancyrandom().randlist;

        }
    }
}
