﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using A;


namespace GUI
{
    public partial class Tradedata : Form
    {
        static public List<object> TradeList = new List<object>();
        //static double[] a=new double[7];
        public Tradedata()
        {
            InitializeComponent();
            comboBox1.Items.Clear();
            MyEntityContainer context = new MyEntityContainer();
            foreach (var instruments in context.Instruments) { comboBox1.Items.Add(instruments.Ticker); }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MyEntityContainer context = new MyEntityContainer();
            int OptionType = (from abs in context.Instruments where abs.Ticker == comboBox1.SelectedItem.ToString() select abs).FirstOrDefault().InstTypeId;
            if (OptionType != 1)// If you want to trade the Option
            {
                short direction;
                double rate = 0;
                double sigma = Form1.v;
                if (checkBox1.Checked) { direction = 1; } else { direction = -1; }
                //select the Instruments' id when they have the same ticker
                int id = (from abs in context.Instruments where abs.Ticker == comboBox1.SelectedItem.ToString() select abs).FirstOrDefault().Id;
                //select the Instruments' Ticker when they have the same ticker
                string tic = (from abs in context.Instruments where abs.Ticker == comboBox1.SelectedItem.ToString() select abs).FirstOrDefault().Ticker;
                //select the Instrument's strike when they have the same ticker
                double strike = (from abs in context.Instruments where abs.Ticker == comboBox1.SelectedItem.ToString() select abs).FirstOrDefault().Strike;
                //select the Instrument's tenor when they have the same ticker
                double tenor = (from abs in context.Instruments where abs.Ticker == comboBox1.SelectedItem.ToString() select abs).FirstOrDefault().Tenor;
                //select the DigitalOption's rebate when they have the same ticker
                double rebate = Convert.ToDouble((from abs in context.Instruments where abs.Ticker == comboBox1.SelectedItem.ToString() select abs).FirstOrDefault().Rebate);
                //select the BarrierOption's barrier when they have the same ticker
                double barrier = Convert.ToDouble((from abs in context.Instruments where abs.Ticker == comboBox1.SelectedItem.ToString() select abs).FirstOrDefault().Barrier);
                //select the Instrument's TypeName in InstType when they have the same id
                string TypeName = (from abs in context.InstTypes where OptionType == abs.Id select abs).FirstOrDefault().TypeName;
                //select the Instrument's IsCall in InstType when they have the same Ticker
                double type = Convert.ToDouble((from abs in context.Instruments where abs.Ticker == comboBox1.SelectedItem.ToString() select abs).FirstOrDefault().IsCall);
                //select the BarrierOption's barriertype when they have the same ticker
                string barriertype = (from abs in context.Instruments where abs.Ticker == comboBox1.SelectedItem.ToString() select abs).FirstOrDefault().BarrierType;
                //calculate the insterest rate
                var ratelist = (from rates in context.InterestRates orderby Math.Abs(rates.Tenor - tenor) descending select rates).ToArray();
                rate = (tenor - ratelist[0].Tenor) * (ratelist[1].Rate - ratelist[0].Rate) / (ratelist[1].Tenor - ratelist[0].Tenor) + ratelist[0].Rate;
                //var rlist = (from rateset in context.InterestRates orderby Math.Abs(tenor - rateset.Tenor) descending select new { rateset.Tenor, rateset.Rate }).ToArray;

                //decide the direction of Barrier option
                int outorin;
                int downorup;
                if (barriertype == "downandout")
                {
                    downorup = -1;
                    outorin = -1;
                }

                if (barriertype == "upandout")
                {
                    downorup = 1;
                    outorin = -1;
                }

                if (barriertype == "dwonadnin")
                {
                    downorup = -1;
                    outorin = 1;

                }

                if (barriertype == "upandin")
                {
                    downorup = 1;
                    outorin = 1;
                }
                else
                {
                    downorup = 0;
                    outorin = 0;
                }
                //find the TimeStamp time
                DateTime timestamp = dateTimePicker2.Value;
                //calculate price from Monto Carlo simulation using the Referenced Library
                double[] a
                    = A.main.calculate(50, strike, rate, Convert.ToInt32(tenor), sigma, rebate, barrier, type, downorup, outorin, TypeName);
                double PaL = -1 * Convert.ToDouble(textBox1.Text) * direction * (Convert.ToDouble(textBox3.Text) - a[0]);//P&L=quantity*(simulated price-closing price)

                context.Trades.Add(new Trade
                {
                    IsBuy = direction,
                    Quantity = Convert.ToDouble(textBox1.Text),
                    Price = Convert.ToDouble(textBox3.Text),
                    InstrumentId = id,
                    SimulatedPrice = Convert.ToString(a[0]),
                    Delta = Convert.ToDouble(textBox1.Text)* a[1],
                    Gamma = Convert.ToDouble(textBox1.Text)*a[2],
                    Theta = Convert.ToDouble(textBox1.Text)*a[3],
                    Vega = Convert.ToDouble(textBox1.Text)*a[4],
                    Rho = Convert.ToDouble(textBox1.Text)* a[5],
                    PL = PaL,
                    Ticker = tic,
                    Timestamp = Convert.ToString(timestamp)
                });
                context.SaveChanges();
                MessageBox.Show("successfully saved");
                refresh();
                //Notice each time you want to refresh the view, you have to clear or re-define the rateList;
                if (refresh() == true) { } else { MessageBox.Show("User inputs are wrong"); };
            }

            else//If you want to trade the stock
            {
                short direction;
                if (checkBox1.Checked) { direction = 1; } else { direction = -1; }
                //select the Instruments's id when they have the same name
                int id = (from abs in context.Instruments where abs.Ticker == comboBox1.SelectedItem.ToString() select abs).FirstOrDefault().Id;
                //select the Instruments' Ticker when they have the same ticker
                DateTime timestamp = dateTimePicker2.Value;
                string tic = (from abs in context.Instruments where abs.Ticker == comboBox1.SelectedItem.ToString() select abs).FirstOrDefault().Ticker;
                Price markprice = (from prices in context.Prices where (prices.InstrumentId == (from instrumentset in context.Instruments where instrumentset.Ticker == comboBox1.SelectedItem.ToString() select instrumentset.Id).FirstOrDefault() && (timestamp > prices.Date)) orderby prices.Date ascending select prices).FirstOrDefault();
                string price = markprice.ClosingPrice;

                context.Trades.Add(new Trade
                {
                    IsBuy = direction,
                    Quantity = Convert.ToDouble(textBox1.Text),
                    Price = Convert.ToDouble(textBox3.Text),
                    InstrumentId = id,
                    Delta = 1,
                    Gamma = 0,
                    Rho = 0,
                    Vega = 0,
                    Theta = 0,
                    SimulatedPrice = Convert.ToString(0),
                    Timestamp = Convert.ToString(timestamp),
                    PL = -1 * Convert.ToDouble(textBox1.Text) * (Convert.ToDouble(direction)) * (Convert.ToDouble(textBox3.Text) - Convert.ToDouble(price)),
                    Ticker = tic
                });

                context.SaveChanges();
                MessageBox.Show("successfully saved");
                refresh();
                //Notice each time you want to refresh the view, you have to clear or re-define the rateList;
                if (refresh() == true) { } else { MessageBox.Show("User inputs are wrong"); }
            }

        }


        static public Boolean refresh()
        {
            MyEntityContainer context = new MyEntityContainer();
            try
            {
                TradeList = new List<object>();
                var trades = from tradeset in context.Trades select new { tradeset.Instrument.Ticker, tradeset.Id, tradeset.IsBuy, tradeset.SimulatedPrice, tradeset.PL, tradeset.Price, tradeset.Quantity, tradeset.Delta, tradeset.Gamma, tradeset.Theta, tradeset.Vega, tradeset.Rho };
                foreach (var tr in trades)
                {
                    TradeList.Add(tr);
                }

                Form1.dataGridView2.DataSource = TradeList;
                return true;
            }
            catch
            {
                return false;
            }
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {

        }

    }
}

