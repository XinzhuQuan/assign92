﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;

namespace GUI
{
    public partial class intrument : Form
    {
        static public List<object> instList = new List<object>();
        public intrument()
        {
            InitializeComponent();
            refresh();
            dataGridView1.DataSource = instList;
            MyEntityContainer context = new MyEntityContainer();
            var st = from instruments in context.Instruments where instruments.InstTypeId == 1 select instruments; 
            foreach(var s in st)
            { comboBox1.Items.Add(s.Ticker); }
        }

        private void result_form_Load(object sender, EventArgs e)
        {

        }
       
        public void button1_Click(object sender, EventArgs e)
        {
            MyEntityContainer context = new MyEntityContainer();
            int calla, b;
            //b = comboBox2.SelectedItem.ToString();
            if (call.Checked) { calla = 1; } else { calla = -1; }
            b = (from insttypeset in context.InstTypes where insttypeset.TypeName == comboBox2.SelectedItem.ToString() select insttypeset).FirstOrDefault().Id;

            context.Instruments.Add(new Instrument
            {
                CompanyName = textBox1.Text,
                Ticker = textBox2.Text,
                Exchange = textBox3.Text,
                Underlying = Convert.ToString(comboBox1.SelectedItem),
                Strike = Convert.ToDouble(textBox5.Text),
                Tenor = Convert.ToDouble(textBox6.Text),
                IsCall = calla,
                InstTypeId = b,
                BarrierType = comboBox3.SelectedItem.ToString(),
               Barrier = Convert.ToDouble(textBox9.Text),
                Rebate = Convert.ToDouble(textBox8.Text)
            });
            context.SaveChanges();
            textBox1.Text = textBox2.Text = textBox3.Text = textBox5.Text = textBox6.Text = textBox8.Text = "";
            MessageBox.Show("successfully saved");
            refresh();
            //Notice each time you want to refresh the view, you have to clear or re-define the rateList;
            if (refresh() == true) { }
            else { MessageBox.Show("User inputs are wrong"); };
            dataGridView1.DataSource = instList;
        }

        static public Boolean refresh()
        {
            MyEntityContainer context = new MyEntityContainer();
            try
            {
                instList = new List<object>();
                var inst = from rateset in context.Instruments select new { rateset.Id,rateset.CompanyName,rateset.Underlying , rateset.Ticker, rateset.IsCall,rateset.Tenor,rateset.Strike,rateset.InstType.TypeName };
                foreach (var a in inst )
                {
                    instList.Add(a);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MyEntityContainer context = new MyEntityContainer();
            int calla, b;
            //b = comboBox2.SelectedItem.ToString();
           
            b = (from insttypeset in context.InstTypes where insttypeset.TypeName == comboBox2.SelectedItem.ToString() select insttypeset).FirstOrDefault().Id;
            context.Instruments.Add(new Instrument
            {
                CompanyName = textBox1.Text,
                Ticker = textBox2.Text,
                Exchange = textBox3.Text,
                Underlying = "None",
                Strike = 0,
                Tenor = 0,
                IsCall = 0,
                InstTypeId = b,
                BarrierType = "It is not a barrier",
                Barrier = 0,
                Rebate = 0
            });
            context.SaveChanges();
            textBox1.Text = textBox2.Text = "";
            MessageBox.Show("successfully saved the stock");
            refresh();
            //Notice each time you want to refresh the view, you have to clear or re-define the rateList;
            if (refresh() == true) { }
            else { MessageBox.Show("User inputs are wrong"); };
            dataGridView1.DataSource = instList;
            var st = from instruments in context.Instruments where instruments.InstTypeId == 1 select instruments;
            foreach (var s in st)
            { comboBox1.Items.Add(s.Ticker); }
        }
    }
}
