﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;

namespace GUI
{
    public partial class Interest_Rate : Form
    {
        static public List<object> rateList = new List<object>();
        public Interest_Rate()
        {
            InitializeComponent();
            refresh();
            dataGridView2.DataSource = rateList;
        }

        public void button1_Click(object sender, EventArgs e)
        { 
            MyEntityContainer context = new MyEntityContainer();
            context.InterestRates.Add(new InterestRate { Rate = Convert.ToDouble(textBox2.Text), Tenor = Convert.ToDouble(textBox1.Text) });
            context.SaveChanges();
            textBox1.Text = textBox2.Text = "";
            MessageBox.Show("successfully saved");
            refresh();
            //Notice each time you want to refresh the view, you have to clear or re-define the rateList;
            if (refresh() == true) { } else { MessageBox.Show("User inputs are wrong"); };
            dataGridView2.DataSource = rateList;
        }

        static public Boolean refresh()
        {
            MyEntityContainer context = new MyEntityContainer();
            try
            {
                rateList = new List<object>();
                var rates = from rateset in context.InterestRates select rateset;
                foreach (var rate in rates)
                {
                    rateList.Add(rate);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }


        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
