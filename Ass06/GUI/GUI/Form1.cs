﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class Form1 : Form
    {
        static public List<object> SubTradeList = new List<object>();
        public Form1()
        {
            InitializeComponent();
            v = Convert.ToDouble(textBox1.Text);
            Tradedata.refresh();
            dataGridView2.DataSource = Tradedata.TradeList;
            refreshsub();
        }

        public static double v;

        private void intrumentTypeToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Instrument_type resultShow = new Instrument_type();
            resultShow.ShowDialog();
        }

        public void Form1_Load(object sender, EventArgs e)
        {

        }

        private void tradeToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Tradedata result = new Tradedata();
            result.ShowDialog();
        }

        private void interestRateToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Interest_Rate rate = new Interest_Rate();
            rate.ShowDialog();
        }

        private void intrumentToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            intrument resultShow = new intrument();
            resultShow.ShowDialog();
        }

        private void historicalPriceToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            HistoricalPrice HistoricalPrice = new HistoricalPrice();
            HistoricalPrice.ShowDialog();
        }
        static public void refreshsub()
        {
            try
            {
                MyEntityContainer context = new MyEntityContainer();
                var selected_rows = dataGridView2.Rows;
                double price = 0;
                double quantity = 0;
                double markprice = 0;
                double delta = 0;
                double gamma = 0;
                double theta = 0;
                double vega = 0;
                double rho = 0;
                double PL = 0;
                for (int idx = 0; idx < selected_rows.Count; idx++)
                {
                    if (selected_rows[idx].Selected)
                    {
                        quantity = quantity + Convert.ToDouble(selected_rows[idx].Cells["Quantity"].Value);
                        price = price + Convert.ToDouble(selected_rows[idx].Cells["Price"].Value);
                        markprice = markprice + Convert.ToDouble(selected_rows[idx].Cells["SimulatedPrice"].Value);
                        PL = PL + Convert.ToDouble(selected_rows[idx].Cells["PL"].Value);
                        delta = delta + Convert.ToDouble(selected_rows[idx].Cells["Delta"].Value);
                        gamma = gamma + Convert.ToDouble(selected_rows[idx].Cells["Gamma"].Value);
                        theta = theta + Convert.ToDouble(selected_rows[idx].Cells["Theta"].Value);
                        vega = vega + Convert.ToDouble(selected_rows[idx].Cells["Vega"].Value);
                        rho = rho + Convert.ToDouble(selected_rows[idx].Cells["Rho"].Value);
                    }
                }
                context.Totals.RemoveRange(context.Totals);
                context.SaveChanges();
                if (price != 0)
                {
                    context.Totals.Add(new Total { TotalPL = PL, TotalData = delta, TotalGamma = gamma, TotalTheta = theta, TotalVega = vega, TotalRho = rho });
                    context.SaveChanges();
                }
                refresh();
                if (refresh() == true) { } else { MessageBox.Show("User inputs are wrong"); }
                dataGridView1.DataSource = SubTradeList;
            }
            catch
            {
            }

        }
        static public Boolean refresh()
        {
            MyEntityContainer context = new MyEntityContainer();
            try
            {
                SubTradeList = new List<object>();
                var subtrades = from subtradeset in context.Totals select new { subtradeset.TotalPL,subtradeset.TotalData, subtradeset.TotalGamma, subtradeset.TotalRho, subtradeset.TotalTheta, subtradeset.TotalVega};
                foreach (var tr in subtrades)
                {
                    SubTradeList.Add(tr);
                }

                return true;
            }
            catch
            {
                return false;
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            MyEntityContainer context = new MyEntityContainer();
            DataGridViewSelectedRowCollection selected_rows = dataGridView2.SelectedRows;
            for (int idx = 0; idx < selected_rows.Count; idx++)
            {
                int id = Convert.ToInt32(selected_rows[idx].Cells["Id"].Value);
                Trade trade = (from trades in context.Trades where trades.Id == id select trades).FirstOrDefault();
                try
                {
                    context.Trades.Remove(trade);
                    context.SaveChanges();
                }
                catch
                {
                }
            }
           Tradedata.refresh();
           Form1.dataGridView2.DataSource = Tradedata.TradeList;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            MyEntityContainer context = new MyEntityContainer();
        }


        private void dataGridView2_CellContentClick(object sender, EventArgs e)
        {
            refreshsub();
        }

        private void priceAnlaysisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PriceA result = new PriceA();
            result.ShowDialog();
        }
    }
}
