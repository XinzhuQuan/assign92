//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GUI
{
    using System;
    using System.Collections.Generic;
    
    public partial class Total
    {
        public int Id { get; set; }
        public double TotalPL { get; set; }
        public double TotalData { get; set; }
        public double TotalGamma { get; set; }
        public double TotalTheta { get; set; }
        public double TotalRho { get; set; }
        public double TotalVega { get; set; }
    }
}
