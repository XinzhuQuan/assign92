﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class HistoricalPrice : Form
    {
        static public List<object> PriceList = new List<object>();
        public HistoricalPrice()
        {
            InitializeComponent();
            InstNameList.Items.Clear();
            MyEntityContainer context = new MyEntityContainer();
            foreach (var instruments in context.Instruments) {InstNameList.Items.Add(instruments.Ticker); }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            MyEntityContainer context = new MyEntityContainer();
           
           int id = (from abs in context.Instruments where abs.Ticker == InstNameList.SelectedItem.ToString() select  abs ).FirstOrDefault().Id;
            context.Prices.Add(new Price { ClosingPrice=textBox1.Text, Date = Convert.ToDateTime(dateTimePicker2.Text),InstrumentId=id});
            context.SaveChanges();
            textBox1.Text =  "";
            MessageBox.Show("successfully saved");
            refresh();
            //Notice each time you want to refresh the view, you have to clear or re-define the rateList;
            if (refresh() == true) { } else { MessageBox.Show("User inputs are wrong"); };
            dataGridView1.DataSource = PriceList;
        }
        static public Boolean refresh()
        {
            MyEntityContainer context = new MyEntityContainer();
            try
            {
               PriceList = new List<object>();
                var rates = from rateset in context.Prices select new { rateset.Id, rateset.Date,rateset.ClosingPrice};
                foreach (var a in rates)
                {
                    PriceList.Add(a);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
