﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class Instrument_type : Form
    {
        static public List<object> TypeList = new List<object>();
        public Instrument_type()
        {
            InitializeComponent();
            refresh();
            dataGridView2.DataSource = TypeList;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MyEntityContainer context = new MyEntityContainer();
            context.InstTypes.Add(new InstType { TypeName = textBox1.Text });
            context.SaveChanges();
            textBox1.Text = "";
            MessageBox.Show("successfully saved");
            refresh();
            //Notice each time you want to refresh the view, you have to clear or re-define the TypeList;
            if (refresh() == true) { } else { MessageBox.Show("User inputs are wrong"); };
            dataGridView2.DataSource = TypeList;
        }
        static public Boolean refresh()
        {
            MyEntityContainer context = new MyEntityContainer();
            try
            {
                TypeList = new List<object>();
                var rates = from ab in context.InstTypes select new {ab.Id,ab.TypeName} ;
                foreach (var rate in rates)
                {
                    TypeList.Add(rate);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
