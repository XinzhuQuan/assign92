﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class PriceA : Form
    {
        static public List<object> HisList= new List<object>();
        public PriceA()
        {
            InitializeComponent();
            comboBox1.Items.Clear();
            MyEntityContainer context = new MyEntityContainer();
            foreach (var instruments in context.Instruments) { comboBox1.Items.Add(instruments.Ticker); }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MyEntityContainer context = new MyEntityContainer();
            int id = (from abs in context.Instruments where abs.Ticker == comboBox1.SelectedItem.ToString() select abs).FirstOrDefault().Id;
            var tic = (from abs in context.Prices where id == abs.Id select abs);
            foreach (var a in tic)
            {
               HisList.Add(a);
            }
            refresh();
            dataGridView1.DataSource = HisList;
        }
        static public Boolean refresh()
        {
            MyEntityContainer context = new MyEntityContainer();
            try
            {
                HisList = new List<object>();
                var rates = from rateset in context.Prices select new { rateset.Id, rateset.Date, rateset.ClosingPrice };
                foreach (var a in rates)
                {
                    HisList.Add(a);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

    }
}
