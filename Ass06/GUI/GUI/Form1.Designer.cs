﻿namespace GUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.intrumentTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.intrumentTypeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.intrumentToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tradeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.interestRateToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.historicalPriceToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.analysisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.priceAnlaysisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            dataGridView1 = new System.Windows.Forms.DataGridView();
            dataGridView2 = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.analysisToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(968, 33);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.intrumentTypeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(50, 29);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // intrumentTypeToolStripMenuItem
            // 
            this.intrumentTypeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.intrumentTypeToolStripMenuItem1,
            this.intrumentToolStripMenuItem1,
            this.tradeToolStripMenuItem1,
            this.interestRateToolStripMenuItem1,
            this.historicalPriceToolStripMenuItem1});
            this.intrumentTypeToolStripMenuItem.Name = "intrumentTypeToolStripMenuItem";
            this.intrumentTypeToolStripMenuItem.Size = new System.Drawing.Size(131, 30);
            this.intrumentTypeToolStripMenuItem.Text = "New";
            // 
            // intrumentTypeToolStripMenuItem1
            // 
            this.intrumentTypeToolStripMenuItem1.Name = "intrumentTypeToolStripMenuItem1";
            this.intrumentTypeToolStripMenuItem1.Size = new System.Drawing.Size(214, 30);
            this.intrumentTypeToolStripMenuItem1.Text = "Intrument type";
            this.intrumentTypeToolStripMenuItem1.Click += new System.EventHandler(this.intrumentTypeToolStripMenuItem1_Click);
            // 
            // intrumentToolStripMenuItem1
            // 
            this.intrumentToolStripMenuItem1.Name = "intrumentToolStripMenuItem1";
            this.intrumentToolStripMenuItem1.Size = new System.Drawing.Size(214, 30);
            this.intrumentToolStripMenuItem1.Text = "Intrument";
            this.intrumentToolStripMenuItem1.Click += new System.EventHandler(this.intrumentToolStripMenuItem1_Click);
            // 
            // tradeToolStripMenuItem1
            // 
            this.tradeToolStripMenuItem1.Name = "tradeToolStripMenuItem1";
            this.tradeToolStripMenuItem1.Size = new System.Drawing.Size(214, 30);
            this.tradeToolStripMenuItem1.Text = "Trade";
            this.tradeToolStripMenuItem1.Click += new System.EventHandler(this.tradeToolStripMenuItem1_Click);
            // 
            // interestRateToolStripMenuItem1
            // 
            this.interestRateToolStripMenuItem1.Name = "interestRateToolStripMenuItem1";
            this.interestRateToolStripMenuItem1.Size = new System.Drawing.Size(214, 30);
            this.interestRateToolStripMenuItem1.Text = "Interest rate";
            this.interestRateToolStripMenuItem1.Click += new System.EventHandler(this.interestRateToolStripMenuItem1_Click);
            // 
            // historicalPriceToolStripMenuItem1
            // 
            this.historicalPriceToolStripMenuItem1.Name = "historicalPriceToolStripMenuItem1";
            this.historicalPriceToolStripMenuItem1.Size = new System.Drawing.Size(214, 30);
            this.historicalPriceToolStripMenuItem1.Text = "Historical Price";
            this.historicalPriceToolStripMenuItem1.Click += new System.EventHandler(this.historicalPriceToolStripMenuItem1_Click);
            // 
            // analysisToolStripMenuItem
            // 
            this.analysisToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.priceAnlaysisToolStripMenuItem});
            this.analysisToolStripMenuItem.Name = "analysisToolStripMenuItem";
            this.analysisToolStripMenuItem.Size = new System.Drawing.Size(88, 29);
            this.analysisToolStripMenuItem.Text = "Analysis";
            // 
            // priceAnlaysisToolStripMenuItem
            // 
            this.priceAnlaysisToolStripMenuItem.Name = "priceAnlaysisToolStripMenuItem";
            this.priceAnlaysisToolStripMenuItem.Size = new System.Drawing.Size(210, 30);
            this.priceAnlaysisToolStripMenuItem.Text = "Price Anlaysis";
            this.priceAnlaysisToolStripMenuItem.Click += new System.EventHandler(this.priceAnlaysisToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Pricing Volatilty(%)";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(157, 44);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(320, 26);
            this.textBox1.TabIndex = 3;
            this.textBox1.Text = "0.5";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Selected Trade";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 290);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 20);
            this.label3.TabIndex = 5;
            this.label3.Text = "All Trades";
            // 
            // dataGridView1
            // 
            dataGridView1.AllowUserToOrderColumns = true;
            dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridView1.Location = new System.Drawing.Point(25, 103);
            dataGridView1.Name = "dataGridView1";
            dataGridView1.RowTemplate.Height = 28;
            dataGridView1.Size = new System.Drawing.Size(851, 161);
            dataGridView1.TabIndex = 6;
            // 
            // dataGridView2
            // 
            dataGridView2.AllowUserToOrderColumns = true;
            dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridView2.Location = new System.Drawing.Point(25, 330);
            dataGridView2.Name = "dataGridView2";
            dataGridView2.RowHeadersVisible = false;
            dataGridView2.RowTemplate.Height = 28;
            dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            dataGridView2.Size = new System.Drawing.Size(851, 163);
            dataGridView2.TabIndex = 7;
            dataGridView2.SelectionChanged += new System.EventHandler(dataGridView2_CellContentClick);
            dataGridView2.MouseClick += new System.Windows.Forms.MouseEventHandler(dataGridView2_CellContentClick);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(688, 47);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(177, 27);
            this.button1.TabIndex = 8;
            this.button1.Text = "Delete rows";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(968, 566);
            this.Controls.Add(this.button1);
            this.Controls.Add(dataGridView1);
            this.Controls.Add(dataGridView2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(dataGridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem intrumentTypeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem intrumentTypeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem intrumentToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tradeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem interestRateToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem historicalPriceToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem analysisToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem priceAnlaysisToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        public static System.Windows.Forms.DataGridView dataGridView1;
        public static System.Windows.Forms.DataGridView dataGridView2;
    }
}

