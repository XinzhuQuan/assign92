﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp4
{
    class Asian : Option
    {

        public double sum = 0;

        public double option_price = 0;
        public double[] Price;




        //  public double dt = Convert.ToDouble(CommonValue.Time) / Convert.ToDouble (CommonValue.steps);//calculte t
        // public double dtt = Convert.ToDouble(CommonValue.Time+CommonValue.increR* CommonValue.Time) / Convert.ToDouble(CommonValue.steps);//calculte the t+0.0001t

        public void GetPrice(double S, double K, double R, double T, double sigma)
        {

            double dt = Convert.ToDouble(T) / Convert.ToDouble(CommonValue.steps);
            double erddt = Math.Exp(R * dt);
            Price = new double[CommonValue.trail * (1 + CommonValue.indicator)];
            double delta1 = 0;
            double t = 0; 
            double cvone = 0;
            Parallel.ForEach(Ienum.Step(0, CommonValue.trail * (1 + CommonValue.indicator), 1), new ParallelOptions { MaxDegreeOfParallelism = CommonValue.thread }, trails =>
            {
                double suma = 0;
                for (int idx = 1; idx <= CommonValue.steps; idx++)
                {   //use monto carlo calculate the stock price
                    //t = (idx - 1) * dt;
                    //   delta1 = BS.delta(CommonValue.type, CommonValue.St[trails, idx-1], CommonValue.Strike, CommonValue.Rate, CommonValue.Sigma, CommonValue.Time, t);
                    // delta2 = BS.delta(CommonValue.type, St2, CommonValue.Strike, CommonValue.Rate, CommonValue.Sigma, CommonValue.Time, t);
                   CommonValue.St[trails, 0] = S;
                    CommonValue.St[trails, idx] = CommonValue.St[trails, idx - 1] * Math.Exp(((R - 0.5 * sigma * sigma) * dt) + (sigma * Math.Sqrt(dt) * CommonValue.rand[trails, idx - 1]));
                    t = (idx - 1) * dt;
                    delta1 = BS.delta(CommonValue.type, CommonValue.St[trails, idx - 1], K, R, sigma, T, t);
                    cvone = cvone + delta1 * (CommonValue.St[trails, idx] - CommonValue.St[trails, idx - 1] * erddt);
                    suma = suma + CommonValue.St[trails, idx];
                }   

                Price[trails] = Math.Max(0, CommonValue.type * ((suma/ CommonValue.steps) - K)); //use the last column price minus strike price

                if (CommonValue.indicator2 == 1)// if it use delta reduction method, the last column of option price should have some changes
                {
                    Price[trails] = Math.Max(0, CommonValue.type * (suma / (CommonValue.steps) - K)) - 1 * cvone;
                }
                cvone = 0;
            });


            for (int trails = 0; trails < CommonValue.trail * (1 + CommonValue.indicator); trails++)
            {
                sum = sum + Price[trails];
            }
            option_price = sum * Math.Exp(-T * R) / (CommonValue.trail * (1 + CommonValue.indicator));




        } }
}
    

