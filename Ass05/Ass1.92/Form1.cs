﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Diagnostics;
using Ass1._92;

namespace WindowsFormsApp4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            CommonValue.Cpu_cores = System.Environment.ProcessorCount;
            textBox3.Text =
                "You have " +
                Convert.ToString(CommonValue.Cpu_cores) +
                " logical cores\n" +
                "May not be the number of your physical (hardware) cores\n";
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }


        private void button1_Click(object sender, EventArgs e)
        {
            progressBar1.Value = 0;
            // obtiain the value from textboxs
            string s = textBox10.Text.Trim();
            string k = textBox11.Text.Trim();
            string r = textBox12.Text.Trim();
            string sigma = textBox13.Text.Trim();
            string t = textBox14.Text.Trim();
            string tr = textBox15.Text.Trim();
            string ty = textBox1.Text.Trim();
            string steps = textBox2.Text.Trim();
            string thread = textBox4.Text.Trim();
            string rebate = textBox5.Text.Trim();
            string barrier = textBox7.Text.Trim();
            CommonValue.Spotprice = Convert.ToDouble(s);
            CommonValue.Strike = Convert.ToDouble(k);
            CommonValue.Rate = Convert.ToDouble(r);
            CommonValue.Sigma = Convert.ToDouble(sigma);
            CommonValue.Time = Convert.ToInt32(t);
            CommonValue.trail = Convert.ToInt32(tr);
            CommonValue.steps = Convert.ToInt32(steps);
            //textBox16.Text = CommonValue.Sigma.ToString();
            if (ty == "call") { CommonValue.type = 1; }
            else { CommonValue.type = -1; }

            if (checkBox1.Checked)
            {
                CommonValue.indicator = 1;
            }
            else
            {
                CommonValue.indicator = 0;
            }

            if (checkBox2.Checked)
            {
                CommonValue.indicator2 = 1;
            }
            else
            {
                CommonValue.indicator2 = 0;
            }

            if (checkBox3.Checked)
            {
                CommonValue.thread = Convert.ToInt32(thread);
            }
            else
            {
                CommonValue.thread = 1;
            }

            if (checkBox4.Checked)
            {
                CommonValue.indicatorA = 1;
            }
            if (checkBox5.Checked)
            {
                CommonValue.indicatorD = 1;
                CommonValue.rebate = Convert.ToDouble(rebate);
            }
            if (checkBox6.Checked)
            {
                CommonValue.indicatorB = 1;
                CommonValue.barrier = Convert.ToDouble(barrier);
            }
            if (checkBox9.Checked)
            {
                CommonValue.indicatorR = 1;

            }
            if (checkBox8.Checked)
            {
                CommonValue.indicatorE = 1;

            }
            if (checkBox7.Checked)
            {
                CommonValue.indicatorL = 1;
            }

            if (checkBox10.Checked)
            {
                CommonValue.downorup = -1;
                CommonValue.outorin = -1;
            }

            if (checkBox11.Checked)
            {
                CommonValue.downorup = 1;
                CommonValue.outorin = -1;
            }

            if (checkBox12.Checked)
            {
                CommonValue.downorup = -1;
                CommonValue.outorin = 1;

            }

            if (checkBox13.Checked)
            {
                CommonValue.downorup = 1;
                CommonValue.outorin = 1;
            }


            Thread C = new Thread(new ThreadStart(scheduler));
            C.Start();

        }
    


            public void scheduler()
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                CommonValue.gene_path();
                double[] C = Simulator.Mon();
                CommonValue.indicatorA = 0;
                CommonValue.indicatorD = 0;
                CommonValue.indicatorR = 0;
                CommonValue.indicatorL = 0;
                CommonValue.indicatorB = 0;
                CommonValue.t1 = sw.Elapsed;
                sw.Stop();
                Program.finished();
            }

            public void finish()
            {
                textBox6.Text =
                   "option price is : " + Convert.ToString(CommonValue.result[0]) + "\r\n" +
                   "delta is : " + Convert.ToString(CommonValue.result[1]) + "\r\n" +
                   "gamma is : " + Convert.ToString(CommonValue.result[2]) + "\r\n" +
                   "theta is : " + Convert.ToString(CommonValue.result[3]) + "\r\n" +
                   "vega is : " + Convert.ToString(CommonValue.result[4]) + "\r\n" +
                   "rho is : " + Convert.ToString(CommonValue.result[5]) + "\r\n" +
                   "SE is  : " + Convert.ToString(CommonValue.result[6]) + "\r\n" +
                   "Time is  : " + Convert.ToString(CommonValue.t1) + "\n";
            }



        }
    } 

    
    

            

 